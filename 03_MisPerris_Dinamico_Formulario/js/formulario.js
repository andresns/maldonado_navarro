
//validacion de letras

function soloLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toString();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
    especiales = [8, 37, 39, 46, 6]; 

    tecla_especial = false
    for(var i in especiales) {
        if(key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla) == -1 && !tecla_especial){
        return false;
      }
}

//validacion de rut

function checkRut(rut) {
    var valor = rut.value.replace('.','');
    valor = valor.replace('-','');
    
    cuerpo = valor.slice(0,-1);
    dv = valor.slice(-1).toUpperCase();
    
    rut.value = cuerpo + '-'+ dv
    
    if(cuerpo.length < 7) { 
        rut.setCustomValidity("RUT Incompleto"); 
        return false;
    }
    
    suma = 0;
    multiplo = 2;
    
    for(i=1;i<=cuerpo.length;i++) {
    
        index = multiplo * valor.charAt(cuerpo.length - i);
        
        suma = suma + index;
        
        if(multiplo < 7) {
            multiplo = multiplo + 1; 
        } 
        else 
        { 
            multiplo = 2; 
        }
  
    }
    
    dvEsperado = 11 - (suma % 11);
    
    dv = (dv == 'K')?10:dv;
    dv = (dv == 0)?11:dv;
    
    if(dvEsperado != dv) { 
        rut.setCustomValidity("RUT Inválido");
        return false; 
    }
    
    rut.setCustomValidity('');
}

//funcion solo numeros

function SoloNumeros(evt){
    if(window.event){
     keynum = evt.keyCode;
    }
    else{
     keynum = evt.which;
    } 
    if((keynum > 47 && keynum < 58) || keynum == 8 || keynum == 13 || keynum == 6 ){
     return true;
    }
    else{
     return false;
    }
   }