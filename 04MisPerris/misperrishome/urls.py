from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'misperrishome'

urlpatterns = [
    path(r'', views.pantalla_inicio, name='pantalla_inicio'),
    path(r'lista_perritos/', views.lista_perritos, name='lista_perritos'),
    path(r'agregar_perrito/', views.agregar_perrito, name='agregar_perrito'),
    url(r'^(?P<id>\d+)/editar/$', views.editar_perrito, name="editar"),
    url(r'^(?P<id>\d+)/adoptar/$', views.adoptar_perrito, name="adoptar"),
    path(r'<id>/eliminar_perrito/', views.eliminar_perrito, name='eliminar_perrito'),
]