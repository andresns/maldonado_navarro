from django.apps import AppConfig


class MisperrishomeConfig(AppConfig):
    name = 'misperrishome'
