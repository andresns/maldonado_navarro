from django.shortcuts import render, redirect, get_object_or_404
from .models import Perrito
from django.contrib.auth.decorators import login_required
from . import forms
from django.contrib import messages
from .forms import ModificarPerrito
from django.views.generic.edit import UpdateView
from .models import Estado

def pantalla_inicio(request):
    return render(request, 'misperrishome/index.html')

def registro_perritos(request):
    return render(request, 'misperrishome/registro_perritos.html')

def lista_perritos(request):
    if request.method == 'POST':
        filtro = request.POST.get('filtro')
        perritos = Perrito.objects.filter(estado__pk=filtro)
    else:
        perritos = Perrito.objects.all().order_by('nombre')
    
    adoptado = get_object_or_404(Estado, pk=2)
    disponible = get_object_or_404(Estado, pk=3)

    return render(request, 'misperrishome/lista_perritos.html', {'perritos' : perritos, 'adoptado':adoptado, 'disponible':disponible})

@login_required(login_url="/cuentas/iniciar_sesion/")
def agregar_perrito(request):
    if request.method == 'POST':
        form = forms.AgregarPerrito(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('misperrishome:lista_perritos')
    else:
        form = forms.AgregarPerrito()
    return render(request, 'misperrishome/agregar_perrito.html', {'form':form})

@login_required(login_url="/cuentas/iniciar_sesion/")
def editar_perrito(request, id=None):
    instance = get_object_or_404(Perrito, id=id)
    form = ModificarPerrito(request.POST or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        return redirect('misperrishome:lista_perritos')
    context = {
        "nombre":instance.nombre,
        "instance":instance,
        "form":form,
    }
    return render(request, 'misperrishome/editar_perrito.html', context)

@login_required(login_url="/cuentas/iniciar_sesion/")
def adoptar_perrito(request, id=None):
    estado = get_object_or_404(Estado, pk=2)
    perrito = get_object_or_404(Perrito, id=id)
    perrito.estado = estado
    perrito.dueno = request.user
    perrito.save(update_fields=["estado"])
    perrito.save(update_fields=["dueno"])
    return redirect('misperrishome:lista_perritos')


@login_required(login_url="/cuentas/iniciar_sesion/")
def eliminar_perrito(request, id=None):
    instance = get_object_or_404(Perrito, id=id)
    instance.delete()
    return redirect('misperrishome:lista_perritos')