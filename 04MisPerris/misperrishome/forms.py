from django import forms
from . import models

class AgregarPerrito(forms.ModelForm):
    class Meta:
        model = models.Perrito
        fields = ['nombre','raza','descripcion','foto']

class ModificarPerrito(forms.ModelForm):
    class Meta:
        model = models.Perrito
        fields = ['nombre','raza','descripcion','foto', 'estado', 'dueno']
