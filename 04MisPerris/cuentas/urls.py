from django.conf.urls import url
from django.urls import path
from . import views
from django.contrib.auth.views import (
    PasswordResetView,
    PasswordResetConfirmView,
    PasswordResetDoneView,
    PasswordResetCompleteView,
)


app_name = 'cuentas'

urlpatterns = [
    url(r'^iniciar_sesion/$', views.iniciar_sesion, name="iniciar_sesion"),
    url(r'^registro_usuario/$', views.registro_usuario, name='registro_usuario'),
    url(r'^cerrar_sesion/$', views.cerrar_sesion, name='cerrar_sesion'),
    url(r'^recuperar_pass/$', views.recuperar_pass, name='recuperar_pass'),
    url(r'^reset/password_reset', PasswordResetView.as_view(), {
        'template_name':'registration/password_reset_form.html', 
        'email_template_name':'registration/password_reset_email.html'}, name="password_reset"),
    url(r'^reset/password_reset_done', PasswordResetDoneView.as_view(), {'template_name':'registration/password_reset_done.html'}, name="password_reset_done"),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', PasswordResetConfirmView.as_view(),  {'template_name':'accounts/password_reset_confirm.html'}, name="password_reset_confirm"),
    url(r'^reset/done', PasswordResetCompleteView.as_view(), {'template_name':'registration/password_reset_complete.html'}, name="password_reset_complete"),
]
