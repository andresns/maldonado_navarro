from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from .forms import SignUpForm

# Create your views here.

def registro_usuario(request):
    if request.method == 'POST': #validamos que se haya submiteado algo
        form = SignUpForm(request.POST) # Se crea instancia del formulario con la informacion de POST
        if form.is_valid(): # validamos formulario
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('misperrishome:pantalla_inicio')
    else:
        form = SignUpForm() # Se crea instancia vacia del formulario
    return render(request, 'cuentas/registro_usuario.html', {'form':form})

def iniciar_sesion(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            # Se inicia sesion
            user = form.get_user()
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                return redirect('misperrishome:pantalla_inicio')
    else:
        form = AuthenticationForm()
    return render(request, 'cuentas/iniciar_sesion.html', {'form':form})

def cerrar_sesion(request):
    if request.method == 'POST':
        logout(request)
        return redirect('misperrishome:pantalla_inicio')


def recuperar_pass(request):
    return render(request, 'cuentas/recuperar_pass.html')